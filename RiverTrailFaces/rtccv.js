﻿"use strict";
/// <reference path="jslib/parallelarray.js" />

var CreateRtCcv = (function(){

     var p_cascade = {};

     function RtCcv(){

     }



    //give a canvas convert the imageData to greyscale.
    RtCcv.prototype.grayscale = function (canvas) {
        var ctx = canvas.getContext("2d");
        var imageData = ctx.getImageData(0, 0, canvas.width, canvas.height);
        var data = imageData.data;
        var pix1, pix2, pix = canvas.width * canvas.height * 4;
        while (pix > 0)
            data[pix -= 4] = data[pix1 = pix + 1] = data[pix2 = pix + 2] = (data[pix] * 0.3 + data[pix1] * 0.59 + data[pix2] * 0.11);
        ctx.putImageData(imageData, 0, 0);
        return canvas;
        
    }

    //rivertrail grayscale function.
    RtCcv.prototype.grayscale_p = function (canvas) {
        var ctx = canvas.getContext("2d");
        var pImageData = new ParallelArray(canvas);
        var pResult = pImageData.combine(2, function f(idx) {
            var pixel = this.get(idx[0], idx[1]);
            var color = pixel[0] * 0.3 + pixel[1] * 0.59 + pixel[2] * 0.11;
            return [color, color, color, pixel[3]];
        });
        ctx.putImageData(pResult, 0, 0);
        return canvas;
    }

    RtCcv.prototype.image_prep = function(image) {
        if (image.tagName.toLowerCase() == "img") {
            var canvas = document.createElement("canvas");
            document.body.appendChild(image);
            canvas.width = image.offsetWidth;
            canvas.style.width = image.offsetWidth.toString() + "px";
            canvas.height = image.offsetHeight;
            canvas.style.height = image.offsetHeight.toString() + "px";
            document.body.removeChild(image);
            var ctx = canvas.getContext("2d");
            ctx.drawImage(image, 0, 0);
            console.log("drawing image");
            return canvas;
        }
        return image;
    }

    //Not sure what this does. possibly a kdtree?
    RtCcv.prototype.array_group = function (seq, gfunc) {
        var i, j;
        var node = new Array(seq.length);
        for (i = 0; i < seq.length; i++)
            node[i] = {
                "parent": -1,
                "element": seq[i],
                "rank": 0
            };
        for (i = 0; i < seq.length; i++) {
            if (!node[i].element)
                continue;
            var root = i;
            while (node[root].parent != -1)
                root = node[root].parent;
            for (j = 0; j < seq.length; j++) {
                if (i != j && node[j].element && gfunc(node[i].element, node[j].element)) {
                    var root2 = j;

                    while (node[root2].parent != -1)
                        root2 = node[root2].parent;

                    if (root2 != root) {
                        if (node[root].rank > node[root2].rank)
                            node[root2].parent = root;
                        else {
                            node[root].parent = root2;
                            if (node[root].rank == node[root2].rank)
                                node[root2].rank++;
                            root = root2;
                        }

                        /* compress path from node2 to the root: */
                        var temp, node2 = j;
                        while (node[node2].parent != -1) {
                            temp = node2;
                            node2 = node[node2].parent;
                            node[temp].parent = root;
                        }

                        /* compress path from node to the root: */
                        node2 = i;
                        while (node[node2].parent != -1) {
                            temp = node2;
                            node2 = node[node2].parent;
                            node[temp].parent = root;
                        }
                    }
                }
            }
        }
        var idx = new Array(seq.length);
        var class_idx = 0;
        for (i = 0; i < seq.length; i++) {
            j = -1;
            var node1 = i;
            if (node[node1].element) {
                while (node[node1].parent != -1)
                    node1 = node[node1].parent;
                if (node[node1].rank >= 0)
                    node[node1].rank = ~class_idx++;
                j = ~node[node1].rank;
            }
            idx[i] = j;
        }
        return { "index": idx, "cat": class_idx };
    }



    RtCcv.prototype.detect_pre = function (_params) {
        console.log("detect_pre start");
        var canvas = _params.canvas;
        var interval = _params.interval;
        var scale = _params.scale;
        var next = _params.next;
        var scale_upto = _params.scale_upto;
        //this is the array of canvases.
        var pyr = new Array((scale_upto + next * 2) * 4);
        var ret = new Array((scale_upto + next * 2) * 4);
        pyr[0] = canvas;
        ret[0] = { "width" : pyr[0].width,
            "height" : pyr[0].height,
            "data" : pyr[0].getContext("2d").getImageData(0, 0, pyr[0].width, pyr[0].height).data };
        var i;
        for (i = 1; i <= interval; i++) {
            pyr[i * 4] = document.createElement("canvas");
            pyr[i * 4].width = Math.floor(pyr[0].width / Math.pow(scale, i));
            pyr[i * 4].height = Math.floor(pyr[0].height / Math.pow(scale, i));
            pyr[i * 4].getContext("2d").drawImage(pyr[0], 0, 0, pyr[0].width, pyr[0].height, 0, 0, pyr[i * 4].width, pyr[i * 4].height);
            ret[i * 4] = { "width" : pyr[i * 4].width,
                "height" : pyr[i * 4].height,
                "data" : pyr[i * 4].getContext("2d").getImageData(0, 0, pyr[i * 4].width, pyr[i * 4].height).data };
        }
        for (i = next; i < scale_upto + next * 2; i++) {
            pyr[i * 4] = document.createElement("canvas");
            pyr[i * 4].width = Math.floor(pyr[i * 4 - next * 4].width / 2);
            pyr[i * 4].height = Math.floor(pyr[i * 4 - next * 4].height / 2);
            pyr[i * 4].getContext("2d").drawImage(pyr[i * 4 - next * 4], 0, 0, pyr[i * 4 - next * 4].width, pyr[i * 4 - next * 4].height, 0, 0, pyr[i * 4].width, pyr[i * 4].height);
            ret[i * 4] = { "width" : pyr[i * 4].width,
                "height" : pyr[i * 4].height,
                "data" : pyr[i * 4].getContext("2d").getImageData(0, 0, pyr[i * 4].width, pyr[i * 4].height).data };
        }
        for (i = next * 2; i < scale_upto + next * 2; i++) {
            pyr[i * 4 + 1] = document.createElement("canvas");
            pyr[i * 4 + 1].width = Math.floor(pyr[i * 4 - next * 4].width / 2);
            pyr[i * 4 + 1].height = Math.floor(pyr[i * 4 - next * 4].height / 2);
            pyr[i * 4 + 1].getContext("2d").drawImage(pyr[i * 4 - next * 4], 1, 0, pyr[i * 4 - next * 4].width - 1, pyr[i * 4 - next * 4].height, 0, 0, pyr[i * 4 + 1].width - 2, pyr[i * 4 + 1].height);
            ret[i * 4 + 1] = { "width" : pyr[i * 4 + 1].width,
                "height" : pyr[i * 4 + 1].height,
                "data" : pyr[i * 4 + 1].getContext("2d").getImageData(0, 0, pyr[i * 4 + 1].width, pyr[i * 4 + 1].height).data };
            pyr[i * 4 + 2] = document.createElement("canvas");
            pyr[i * 4 + 2].width = Math.floor(pyr[i * 4 - next * 4].width / 2);
            pyr[i * 4 + 2].height = Math.floor(pyr[i * 4 - next * 4].height / 2);
            pyr[i * 4 + 2].getContext("2d").drawImage(pyr[i * 4 - next * 4], 0, 1, pyr[i * 4 - next * 4].width, pyr[i * 4 - next * 4].height - 1, 0, 0, pyr[i * 4 + 2].width, pyr[i * 4 + 2].height - 2);
            ret[i * 4 + 2] = { "width" : pyr[i * 4 + 2].width,
                "height" : pyr[i * 4 + 2].height,
                "data" : pyr[i * 4 + 2].getContext("2d").getImageData(0, 0, pyr[i * 4 + 2].width, pyr[i * 4 + 2].height).data };
            pyr[i * 4 + 3] = document.createElement("canvas");
            pyr[i * 4 + 3].width = Math.floor(pyr[i * 4 - next * 4].width / 2);
            pyr[i * 4 + 3].height = Math.floor(pyr[i * 4 - next * 4].height / 2);
            pyr[i * 4 + 3].getContext("2d").drawImage(pyr[i * 4 - next * 4], 1, 1, pyr[i * 4 - next * 4].width - 1, pyr[i * 4 - next * 4].height - 1, 0, 0, pyr[i * 4 + 3].width - 2, pyr[i * 4 + 3].height - 2);
            ret[i * 4 + 3] = { "width" : pyr[i * 4 + 3].width,
                "height" : pyr[i * 4 + 3].height,
                "data" : pyr[i * 4 + 3].getContext("2d").getImageData(0, 0, pyr[i * 4 + 3].width, pyr[i * 4 + 3].height).data };
        }
       return [ret];
    };

    RtCcv.prototype.cascade_prep = function(cascade){
        //get max sizes
        var max_feature_count=0;
        var max_pn_size=0;
        for(var i=0;i<cascade.stage_classifier.length;i++){
            var current_feature_count = cascade.stage_classifier[i].count;
            max_feature_count=current_feature_count>max_feature_count?current_feature_count:max_feature_count;
            for(var j=0;j<cascade.stage_classifier[i].feature.length;j++)
            {
                var current_pn_size=cascade.stage_classifier[i].feature[j].size;
                max_pn_size=current_pn_size>max_pn_size?current_pn_size:max_pn_size;
            }
        }

        //create the ParallelArray
        var buffer = function(size,origArray){
            var ar=[];
            for(var i=0;i<size;i++){
                if(i<origArray.length){
                    ar[i]= origArray[i];
                } else {
                    ar[i] = 0;
                }
            }
            return ar;
        }

        var XArray = new Array(cascade.count);
        var non_parray = [];
        for(var k=0;k<cascade.count;k++){
            var sc = cascade.stage_classifier[k];
            for(var d=0;d<sc.count;d++){
                non_parray[d] = [
                    buffer(max_pn_size,[sc.feature[d].size]),
                    buffer(max_pn_size,sc.feature[d].px),
                    buffer(max_pn_size,sc.feature[d].py),
                    buffer(max_pn_size,sc.feature[d].pz),
                    buffer(max_pn_size,sc.feature[d].nx),
                    buffer(max_pn_size,sc.feature[d].ny),
                    buffer(max_pn_size,sc.feature[d].nz)];
            }


            XArray[k] = {
                count:sc.count,
                threshold:sc.threshold,
                pArrays:new ParallelArray(non_parray),
                alpha:new ParallelArray(sc.alpha)}
        }

        p_cascade = {width:cascade.width,height:cascade.height,count:cascade.count,feature_pa:XArray};
    }


    RtCcv.prototype.detect_core = function (pyr,shared) {
        console.log("detect_core start");
        var cascade = p_cascade;
        var interval = shared.interval;
        var scale = shared.scale;
        var next = shared.next;
        var scale_upto = shared.scale_upto;
        var i, j, k, x, y, q;
        var scale_x = 1, scale_y = 1;
        var dx = [0, 1, 0, 1];
        var dy = [0, 0, 1, 1];
        var seq = [];
        console.log("detect_core section A");
        for (i = 0; i < scale_upto; i++) {
            var qw = pyr[i * 4 + next * 8].width - Math.floor(cascade.width / 4);
            var qh = pyr[i * 4 + next * 8].height - Math.floor(cascade.height / 4);
            var step = [pyr[i * 4].width * 4, pyr[i * 4 + next * 4].width * 4, pyr[i * 4 + next * 8].width * 4];
            console.log("detect_core section A2");
            var p_step = new ParallelArray(step);
            console.log("detect_core section A3");
            var paddings = [pyr[i * 4].width * 16 - qw * 16,
                            pyr[i * 4 + next * 4].width * 8 - qw * 8,
                            pyr[i * 4 + next * 8].width * 4 - qw * 4];
            var feature_lst = [];
            for (j = 0; j < cascade.count; j++) {

                var orig_feature = cascade.feature_pa[j].pArrays;
                var t_size = orig_feature.get(0,0,0);
                console.log("test:"+t_size.toString());
                feature_lst[j] = new ParallelArray(t_size,function(k,o,stp){

                    var size = o.get(k,1).length;
                    var ppx = [];
                    var pnx = [];
                    for(var q=0;q<size;q++){
                        ppx[q] =  o.get(k,1,q) * 4 + o.get(k,2,q) * stp.get(o.get(k,3,q));
                        pnx[q] =  o.get(k,4,q) * 4 + o.get(k,5,q) * stp.get(o.get(k,6,q));
                    }
                    return [o.get(k,0),

                        new ParallelArray(ppx),
                        o.get(k,3),
                        new ParallelArray(pnx),
                        o.get(k,6)
                    ];
                },orig_feature,p_step);


            }
            console.log("detect_core section B");
            for (q = 0; q < 4; q++) {
                var u8 = [pyr[i * 4].data, pyr[i * 4 + next * 4].data, pyr[i * 4 + next * 8 + q].data];
                var u8o = [dx[q] * 8 + dy[q] * pyr[i * 4].width * 8, dx[q] * 4 + dy[q] * pyr[i * 4 + next * 4].width * 4, 0];
                for (y = 0; y < qh; y++) {
                    for (x = 0; x < qw; x++) {
                        var sum = 0;
                        var flag = true;
                        for (j = 0; j < cascade.feature_pa.length; j++) {
                            sum = 0;
                            var alpha = cascade.feature_pa[j].alpha;
                            var feature = feature_lst[j];

                            //start paralize
                            //px=1,pz=2,nx=3,nz=4
                            var sum_p = feature.combine(function(idx,alpha_p,u8_,u8o_){
                                var rSum;
                                var feature_k = this.get(idx);
                                var p,pmin = u8_[feature_k.get(2,0)][u8o_[feature_k.get(2,0)]+  feature_k.get(1,0)];
                                var n,nmax = u8_[feature_k.get(4,0)][u8o_[feature_k.get(4,0)]+  feature_k.get(3,0)];
                                if(pmin<=nmax){
                                    rSum = alpha_p.get(idx*2);
                                } else {
                                    var f,shortcut = true;
                                    for(f = 0; f<feature_k.get(0,0);f++){
                                        if(feature_k.get(2,f) >= 0){
                                            p = u8_[feature_k.get(2,f)][u8o_[feature_k.get(2,f)]+feature_k.get(1,f)];
                                            if(p<pmin) {
                                                if(p<= nmax) {
                                                    shortcut = false;
                                                    break;
                                                }
                                                pmin = p;
                                            }
                                        }
                                        if(feature_k.get(4,f) >=0) {
                                            n = u8_[feature_k.get(4,f)][u8o_[feature_k.get(4,f)]+feature_k.get(3,f)];
                                            if(n>nmax){
                                                if(pmin<=n){
                                                    shortcut = false;
                                                    break;
                                                }
                                                nmax = n;
                                            }
                                        }
                                    }
                                    rSum = (shortcut)? alpha_p.get(idx*2+1) : alpha_p.get(idx*2);
                                }
                                return rSum;
                            },alpha,u8,u8o);

                            sum = sum_p.reduce(function(a,b){ return a+b;});



                            //end paralize

                            if (sum < cascade.feature_pa[j].threshold) {
                                flag = false;
                                break;
                            }
                        }
                        if (flag) {
                            seq.push({"x" : (x * 4 + dx[q] * 2) * scale_x,
                                "y" : (y * 4 + dy[q] * 2) * scale_y,
                                "width" : cascade.width * scale_x,
                                "height" : cascade.height * scale_y,
                                "neighbor" : 1,
                                "confidence" : sum});
                        }
                        u8o[0] += 16;
                        u8o[1] += 8;
                        u8o[2] += 4;
                    }
                    u8o[0] += paddings[0];
                    u8o[1] += paddings[1];
                    u8o[2] += paddings[2];
                }
            }
            scale_x *= scale;
            scale_y *= scale;
            console.log("detect_core section Done");
        }
        return seq;
    };

    RtCcv.prototype.detect_post = function (seq,shared) {
        console.log("detect_post start");
        var min_neighbors = shared.min_neighbors;
        var cascade = shared.cascade;
        var interval = shared.interval;
        var scale = shared.scale;
        var next = shared.next;
        var scale_upto = shared.scale_upto;
        var i, j;
        //for (i = 0; i < cascade.pArrays.length; i++)
        //    cascade.stage_classifier[i].feature = cascade.stage_classifier[i].orig_feature;
        seq = seq[0];
        if (!(min_neighbors > 0))
            return seq;
        else {
            var result = ccv.array_group(seq, function (r1, r2) {
                var distance = Math.floor(r1.width * 0.25 + 0.5);

                return r2.x <= r1.x + distance &&
                    r2.x >= r1.x - distance &&
                    r2.y <= r1.y + distance &&
                    r2.y >= r1.y - distance &&
                    r2.width <= Math.floor(r1.width * 1.5 + 0.5) &&
                    Math.floor(r2.width * 1.5 + 0.5) >= r1.width;
            });
            var ncomp = result.cat;
            var idx_seq = result.index;
            var comps = new Array(ncomp + 1);
            for (i = 0; i < comps.length; i++){
                comps[i] = {"neighbors" : 0,
                    "x" : 0,
                    "y" : 0,
                    "width" : 0,
                    "height" : 0,
                    "confidence" : 0};
            }

            // count number of neighbors
            for(i = 0; i < seq.length; i++){
                var r1 = seq[i];
                var idx = idx_seq[i];

                if (comps[idx].neighbors == 0){
                    comps[idx].confidence = r1.confidence;
                }

                ++comps[idx].neighbors;

                comps[idx].x += r1.x;
                comps[idx].y += r1.y;
                comps[idx].width += r1.width;
                comps[idx].height += r1.height;
                comps[idx].confidence = Math.max(comps[idx].confidence, r1.confidence);
            }

            var seq2 = [];
            // calculate average bounding box
            for(i = 0; i < ncomp; i++){
                var n = comps[i].neighbors;
                if (n >= min_neighbors)
                    seq2.push({"x" : (comps[i].x * 2 + n) / (2 * n),
                        "y" : (comps[i].y * 2 + n) / (2 * n),
                        "width" : (comps[i].width * 2 + n) / (2 * n),
                        "height" : (comps[i].height * 2 + n) / (2 * n),
                        "neighbors" : comps[i].neighbors,
                        "confidence" : comps[i].confidence});
            }

            var result_seq = [];
            // filter out small face rectangles inside large face rectangles
            for(i = 0; i < seq2.length; i++){
                var r1 = seq2[i];
                var flag = true;
                for(j = 0; j < seq2.length; j++){
                    var r2 = seq2[j];
                    var distance = Math.floor(r2.width * 0.25 + 0.5);

                    if(i != j &&
                        r1.x >= r2.x - distance &&
                        r1.y >= r2.y - distance &&
                        r1.x + r1.width <= r2.x + r2.width + distance &&
                        r1.y + r1.height <= r2.y + r2.height + distance &&
                        (r2.neighbors > Math.max(3, r1.neighbors) || r1.neighbors < 3))
                    {
                        flag = false;
                        break;
                    }
                }

                if(flag){
                    result_seq.push(r1);
                }
            }
            console.log("detect_post stop");
            return result_seq;
        }
    };

    RtCcv.prototype.detect_objects  = function (_params) {

            var shared={};
            shared.canvas = _params.canvas;
            shared.interval = _params.interval;
            shared.min_neighbors = _params.min_neighbors;
            shared.cascade = _params.cascade;
            shared.scale = Math.pow(2, 1 / (_params.interval + 1));
            shared.next = _params.interval + 1;
            shared.scale_upto = Math.floor(Math.log(Math.min(_params.canvas.width / _params.cascade.width, _params.canvas.height / _params.cascade.height)) / Math.log(shared.scale));
            return this.detect_post(this.detect_core(this.detect_pre(shared)[0],shared),shared);
    }

    var obj = new RtCcv();
    return obj;
})


