﻿/// <reference path="jquery-1.9.1.min.js" />
/// <reference path="camera.js" />
//RiverTrailFaces will be set to the constructor.
var CreateRiverTrailFaces = function ()
{

    var CameraModule = new Camera();
    CameraModule.init();


    var vid = $("#srcVideo")[0];
    var canvas = $("#canvas_view")[0];

    //constructor with public properties
    function RiverTrailFaces(){
        this.name = "RiverTrailFaces";
    }

    var obj = new RiverTrailFaces();
    
    RiverTrailFaces.prototype.update = function ()
    {

        if (CameraModule.stopped == false) {
            window.requestAnimationFrame(this.update.bind(this));
            canvas.width = vid.videoWidth;
            canvas.height = vid.videoHeight;
            var ctx = $('#canvas_view')[0].getContext('2d')
            ctx.drawImage(vid, 0, 0);
            var imgd = ctx.getImageData(0, 0, canvas.width, canvas.height);
            var pix = imgd.data;

            // Loop over each pixel and invert the color.
            for (var i = 0, n = pix.length; i < n; i += 4) {
                var pi = pix[i];
                pix[i] = parseInt(pix[i]*0.95); // red
                //pix[i + 1] = 255 - pix[i + 1]; // green
                pix[i + 2] = parseInt(pix[i+2]*0.55); // blue
                // i+3 is alpha (the fourth element)
            }

            // Draw the ImageData at the given (x,y) coordinates.
            ctx.putImageData(imgd, 0, 0);
        }
    }

    return obj;
    
};


window.onload = function () {
    var rtf = CreateRiverTrailFaces();
    rtf.update();
}