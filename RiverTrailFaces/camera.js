﻿/// <reference path="jquery-1.9.1.min.js" />
(function () {
    var lastTime = 0;
    var vendors = ['webkit', 'moz'];
    for (var x = 0; x < vendors.length && !window.requestAnimationFrame; ++x) {
        window.requestAnimationFrame = window[vendors[x] + 'RequestAnimationFrame'];
        window.cancelAnimationFrame =
          window[vendors[x] + 'CancelAnimationFrame'] || window[vendors[x] + 'CancelRequestAnimationFrame'];
    }

    if (!window.requestAnimationFrame)
        window.requestAnimationFrame = function (callback, element) {
            var currTime = new Date().getTime();
            var timeToCall = Math.max(0, 16 - (currTime - lastTime));
            var id = window.setTimeout(function () { callback(currTime + timeToCall); },
              timeToCall);
            lastTime = currTime + timeToCall;
            return id;
        };

    if (!window.cancelAnimationFrame)
        window.cancelAnimationFrame = function (id) {
            clearTimeout(id);
        };
}());

var Camera = function () {
    
    this._this = this;
    this.stopped = false;
    this.init = function () {
       
        
        navigator.getUserMedia = (navigator.getUserMedia || navigator.webkitGetUserMedia || navigator.mozGetUserMedia || navigator.msGetUserMedia || undefined);
        
        if (navigator.getUserMedia) {
            navigator.getUserMedia({ video: true},
                function (stream) {
                    var video = $("#srcVideo")[0];
                    console.log('starting')
                    video.src = window.URL.createObjectURL(stream);
                    video.onloadedmetadata = function (e) {
                        //do something on success.
                        video.play();
                        console.log('loaded!')
                    };
                    video.onabort = function (e)
                    {
                        _this.stopped = true;
                    }
                },
                function (e) {
                    console.log('error');
                });
        }
    }

    
}